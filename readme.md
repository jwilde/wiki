# Applications Team Wiki Source Repository

⚠️🚨☠️ Do NOT edit the Wiki using Gitlab's provided WYSIWYG editor ☠️🚨⚠️

- Changes are synced from this git repository into the Wiki's git repository
- Any changes made to the Wiki directly will be overwritten by changes made to this Git repository
- Fork and use the usual merge-request flow instead
- I'm sorry it has to be this way

**Wiki**:  https://gitlab.torproject.org/tpo/applications/wiki/-/wikis/home
