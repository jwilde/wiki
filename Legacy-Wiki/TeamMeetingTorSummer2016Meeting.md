### Monday - 30 min meeting
Introduction of topics to be discussed on Thursday and discuss any other session, topic, patch to hack on for the team to prioritize during the week.

### Thursday - 1 to 1:30 hr meeting
 * Review deliverables from grant proposals and organize roadmap - to the extend that is possible to do it.
   * https://trac.torproject.org/projects/tor/wiki/org/roadmaps/TorBrowser
   * DRL deliverables related to mobile (ps: not all is for orfox) https://storm.torproject.org/shared/VyN13x_da_YEIKarjf-pf8Hk7wh0tAL0Id8bc9nJi31
 * Discuss the applications team - what unify us, how we want it to be.
 * Team retrospective - let's talk about how we have done since the last dev meeting and set things we want to fix moving forward
