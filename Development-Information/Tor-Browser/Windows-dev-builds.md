The official way of [building Tor Browser](Development-Information/Tor-Browser/Building) is to use tor-browser-build.

However, doing daily development there would be time consuming.
Therefore, we just use Firefox's build system to build incremental builds, and we copy the updated binaries on top of an existing Tor Browser installation.

The process is consolidated and explained [in this page](Development-Information/Tor-Browser/dev-Build) for Linux and macOS.

The same idea works also on Windows, but setting up a Firefox development environment on Windows is more involved than other platforms.

Moreover, there is a difference on the toolchain and SDKs used for official Firefox builds and for Tor Browser builds.
LLVM/Clang do not provide a C runtime, it can either use Microsoft's CRT (the one included with MSVC) or mingw.
Mozilla uses the former, we use the latter.
Notice that this is different from macOS: while Apple's Clang is different from LLVM's Clang, we use Apple's SDK to build Tor Browser also when cross compiling.

All things considered, we found that cross-compiling from Linux to Windows works quite well.

# Prerequisites

Cross-compiling from Linux to Windows requires a mingw-w64-clang toolchain, a Rust compiler that can target `x86_64-pc-windows-gnu` (or `i686-pc-windows-gnu`) and a few more tools.

We get them from tor-browser-build, even though there are other ways to get them.

## Option 1: make a `firefox` build fail

The easiest way to insert an `exit 1` in `project/firefox/build`.
It might make sense to do it at the beginning of the script, e.g., before setting `distdir`.

At that point, start a normal firefox build (e.g., `make torbrowser-testbuild-windows-x86_64` or `rbm/rbm build firefox --target alpha --target torbrowser-windows-x86_64`).
The build should fail and you should get the debug shell.

### Option 1.1 snapshot its container

You can snapshot that container with

```sh
rbm/container archive <container-dir> some-tarball.tar
```

and then extract it to make it persistent with

```sh
rbm/container extract <persistent-container-dir> some-tarball.tar
```

`<container-dir>` should be the only directory looking a Linux filesystem in `tmp` (you should check all the various directory to find it).

`<persistent-container-dir>` can be whatever you prefer.

After that, you can leave the debug shell and enter the persistent container instead with this command:

```sh
rbm/container run --chroot <persistent-container-dir> -- /bin/bash
```

It will open a root shell in the Windows container.

The first time, you might want to install `git`, to get a `tor-browser.git` clone:

```sh
apt update
apt install git
```

Then, you shouldn't do anything else as root, but switch to an unprivileged user, instead:

```sh
TERM=xterm-256color su -l rbm
```

Before doing anything, you will need to add all the tools to your `$PATH`:

```bash
export PATH=/var/tmp/dist/mingw-w64-clang/bin:/var/tmp/dist/fxc2/bin:/var/tmp/dist/rust/bin:/var/tmp/dist/cbindgen:/var/tmp/dist/nasm/bin:/var/tmp/dist/node/bin:$PATH
```

### Option 1.2: copy only the relevant tools

The `build` script in the working directory of the debug shell setups a certain number of tools (Clang, Rust, etc...).
Their archived are in the same directory, but you can move them to something easier to find from outside.

So, you can extract either that directory, or the tarballs you need.

You can use the `rbm/container get` tool (more information about that in `rbm/container usage get`) or just `cp` to some directory (but you might need to adjust the permissions on the debug shell to access them).

Most of the tools we use don't hardcode the `/var/tmp/dist` directory, so you can extract them wherever you want, and you don't even need to use a container. However, you will need to install wine in your machine.

## Option 2: go through `input_files` in `projects/firefox/config`

You can also go through the list of projects that are added to the build container in its configuration file.

Then, you can get the name of their artifact with

```sh
rbm/rbm showconf <project> filename --target alpha --target torbrowser-windows-x86_64
```

and you will find that artifact in `out/<project>/<filename>`.

Then this option is very similar to 1.2.

At the time of writing, the needed projects are:

- `cbindgen` (the one you use for Linux builds should also work)
- [`fxc2`](https://github.com/mozilla/fxc2) (just a cpp file, once you have a Windows compiler you can easily get it without tor-browser-build)
- `mingw-w64-clang` (Martin Storsjö's [prebuilt binaries](https://github.com/mstorsjo/llvm-mingw/releases) might work as well, but not tested)
- `nasm` (we don't specify a target in its configure file, so installing it from your distro should also work)
- `node` (but Node from your distro should work as well)
- `rust` (a Rust compiler installed with `rustup` might work, as long as you add the Windows target, but we have never tried this way)

But we also have the `wasi-sysroot`, which you might want to enable, if you want to be as close as possible to the official builds.

# Building

Once you've set your environment (i.e., the Windows tools are in `$PATH`), you can switch to building.

## Mozconfig

We don't have a `-dev` mozconfig fore Windows, because we don't build for development purposes on Windows very often.

Also, you will need to add a few settings for cross compilation.

This `mozconfig` works at the time of writing (with browsers based on 115.x ESR):

<details><summary>mozconfig</summary>

```bash
CROSS_COMPILE=1

HOST_CC="clang"
HOST_CXX="clang++"
CC="x86_64-w64-mingw32-clang"
CXX="x86_64-w64-mingw32-clang++"
CXXFLAGS="-fms-extensions"
AR=llvm-ar
RANLIB=llvm-ranlib

# For Stylo (not sure still needed)
clang_path=/var/tmp/dist/mingw-w64-clang
BINDGEN_CFLAGS="-I$clang_path/x86_64-w64-mingw32/include/c++/v1 -I$clang_path/x86_64-w64-mingw32/include"

ac_add_options --target=x86_64-w64-mingw32
ac_add_options --with-toolchain-prefix=x86_64-w64-mingw32-

# Disable stuff that doesn't work with mingw, or that we in general don't want
ac_add_options --disable-bits-download
ac_add_options --disable-maintenance-service
ac_add_options --disable-default-browser-agent
ac_add_options --disable-update-agent
ac_add_options --disable-notification-server

# WebRTC used not to work on mingw. We have some patches to fix it for Mullvad
# Browser, but we still keep WebRTC disabled on Tor Browser and upstream has not
# taken the patches, yet.
#ac_add_options --enable-webrtc
ac_add_options --disable-webrtc

# You can also point it to a WASI sysroot, if you have one
ac_add_options --without-wasm-sandboxed-libraries

# Useful for debugging, but it will make the experience extremely slow, so not
# really useful to check only if a feature works (unless you want to make sure
# you are not triggering assertions)
#ac_add_options --enable-debug
#ac_add_options --enable-debug-js-modules
#ac_add_options --disable-optimize
#export MOZ_COPY_PDBS=1
ac_add_options --enable-strip

# Updater-only (probably something you don't need for this kind of builds).
# It should work also on Firefox (we uplifted this) but it didn't make into
# ESR 115.
#ac_add_options --enable-nss-mar

# Tor Browser-specific options. Do not enable if you're testing Firefox patches
# for uplift.
mk_add_options MOZ_APP_DISPLAYNAME="Tor Browser"
ac_add_options --with-base-browser-version=devbuild
ac_add_options --with-relative-data-dir=TorBrowser/Data/Browser
# ac_add_options --with-relative-data-dir=../Data
```

</details>

After you've set it, you can run `./mach configure` normally.

## Build

Build normally with 

```sh
./mach build
```

## Package to run on a Windows host

Since we need to run on a Windows host, we need to package the various binaries.
Mozilla's official packaging steps involves creating an installer, but we're not interested in that.

Instead, we gather all the files in `obj-x86_64-w64-mingw32/dist/firefox` with this command:

```sh
./mach build stage-package
```

That's the only directory you need to somehow copy to a Windows machine (e.g., create a zip file).

However, if you also wish to debug this build, you should also copy other PDBs that aren't included there (the most important one is `obj-x86_64-w64-mingw32/toolkit/library/build/xul.pdb`) and the directory of generated headers (`obj-x86_64-w64-mingw32/dist/include/`; that directory contains a lot of symlinks, be sure to follow them when copying the files).

# Running

The first time you will need to download a regular Tor Browser installer and run it.

After that, every time you generate a new `firefox` directory with the above step, you can just extract it on top of `.../Tor Browser/Browser`.

You can easily write a script to automate this process.

<details><summary>Python deploy script</summary>

```python
from pathlib import Path
from io import BytesIO
from urllib.request import urlopen
from zipfile import ZipFile

data = BytesIO()
# This zip file should include the content of the firefox directory,
# without the firefox directory itself.
url = "https://.../firefox.zip"
with urlopen(url) as f:
    size = 0
    while chunk := f.read(1024 * 1024):
        data.write(chunk)
        # Needed only to write a progress
        size += len(chunk)
        print(f"Read {size / 1024 / 1024}MB")

with ZipFile(data) as z:
    z.extractall(path=(Path(__file__).parent / "Browser"))
```

</details>

Then you can run by opening `firefox.exe` either directly or through the link created by the installer.
The `mozconfig` from above doesn't customize the branding, so you should get a Mozilla Nighlty icon instead of Tor Browser.

## Debugging

You can also debug a build obtained in this way.
If you've copied also the PDBs, you can follow [the instructions for official builds](Development-Information/Tor-Browser/Debugging-and-Triaging#windows).

Local dev build enables you do quickly add break points programmatically without waiting a full build.
You can just add this call where you want to break:

```c++
__debugbreak();
```

When you hit them, Windows will show you this window:

![Debug prompt](uploads/72ef2f181417ee2e291fbd1f7075b176/Screenshot_from_2024-03-06_12-47-10.png)

If this does not happen (and the program just closes), you will need to look for `HKCU\Software\Microsoft\Windows\Windows Error Reporting` in regedit, and add a new `DWORD` value: `DontShowUI = 0`.