## A Note on JDKs

we use the path the JDK path `/usr/lib/jvm/java-11-openjdk-amd64` in this doc. Use what ever is on your system, in some cases the arch is dropped and it's `/usr/lib/jvm/java-11-openjdk`. Also this has been tested with and works with `/usr/lib/jvm/java-17-openjdk-amd64` as well. For Mac (if using homebrew) its `/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/` for arm64 and `/usr/local/Cellar/openjdk@11/11.0.21/libexec/openjdk.jdk/Contents/Home` for x86_64.

## Prepping Tools (skip this section for mac, we get our clang via Android Studio)

Get a copy of [tor-browser-build](https://gitlab.torproject.org/tpo/applications/tor-browser-build/) and checkout the branch that matches your build target 

*(2023.07.13: for 115.x.0-13.0 we still need [pierov:bug_40855_android](https://gitlab.torproject.org/pierov/tor-browser-build/-/tree/bug_40855_android?ref_type=heads) please delete once merged to tbb propper)*

We need to build a clang compiler for our target, aarch64, and we need a few armv7 tools for packaging. The compiler we will use is `clang` and we also need a few extras found in `llvm-runtimes`

```
./rbm/rbm build clang --target alpha --target torbrowser-android-aarch64
./rbm/rbm build llvm-runtimes --target alpha --target torbrowser-android-aarch64
./rbm/rbm build llvm-runtimes --target alpha --target torbrowser-android-armv7
```

Then we need to assemble our compiler, but layering the llvm-runtimes on it.

```
cd out/clang
tar -xf clang-16.x.y-hhhhhh.tar.gz
mv clang clang-16.x.y-arm
tar -xzf ./../llvm-runtimes/llvm-runtimes-16.x.y-aarch64-hhhhhh.tar.gz
tar -xzf ./../llvm-runtimes/llvm-runtimes-16.x.y-armv7-hhhhhh.tar.gz
cp -r llvm-runtimes/lib/clang/16/lib/ clang-16.x.y-arm/lib/clang/16/
```

And now both arm7 and aarch64 llvm-runtimes are installed in your clang and it's ready for use.

## Building Geckoview

Geckoview is the core browser engine. It will be compiled for android and prepared for packaging in an android browser.

clone a copy of [tor-browser](https://gitlab.torproject.org/tpo/applications/tor-browser) and check out the relevant branch (eg: tor-browser-115.2.1-13.5-1)

make a copy of `tools/geckoview/android-env-$OS-template.sh` as `tools/geckoview/android-env.sh` and fill it accordingly to your setup with the following


For Linux:
```
export MOZ_BUILD_DATE=20230710165010 # This should match the data in [firefox-android](https://gitlab.torproject.org/tpo/applications/firefox-android)/android-components/plugins/dependencies/src/main/java/Gecko.kt ~ln 12's def of the variable *version*, the date component
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export ANDROID_HOME=$HOME/.mozbuild/android-sdk-linux/ # or $HOME/Android/Sdk/ # Or .../android-toolchain/android-sdk-linux if you extract android-toolchain from tor-browser-build
export ANDROID_NDK_HOME=$ANDROID_HOME/ndk/23.2.8568313/ # for 115esr
export GRADLE_HOME=/FULL/PATH/TO/tor-browser-build/out/gradle/gradle-7.5.1 # Or the version that we currently use
export LOCAL_DEV_BUILD=1
export PATH=/FULL/PATH/TO/tor-browser-build/out/clang/clang-16.x.y-arm/bin/:$PATH # prepend our newly built and assembled clang to the path so it gets used to build geckoview
```

For Mac: (default is arm64, note the different paths for x86_64). Make sure to download the appropriate NDK's via Android Studio's SDK manager (you can select the specific version by checking the box in the bottom right that says "Show Package Details") ![Screenshot_2023-11-07_at_14.04.00](uploads/bb5eaa40b39284829fb3f21377c61b6f/Screenshot_2023-11-07_at_14.04.00.png)
```
export MOZ_BUILD_DATE=20230710165010 # This should match the data in [firefox-android](https://gitlab.torproject.org/tpo/applications/firefox-android)/android-components/plugins/dependencies/src/main/java/Gecko.kt ~ln 12's def of the variable *version*, the date component
export JAVA_HOME=/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/ # for arm64. Or JAVA_HOME=/usr/local/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/ for x86_64.
export ANDROID_HOME=$HOME/Library/Android/sdk # or $HOME/.mozbuild/android-sdk-macosx/
export ANDROID_NDK_HOME=$ANDROID_HOME/ndk/23.2.8568313 # will need to download NDK 23.2.8568313 via android studio
export GRADLE_HOME=/opt/homebrew/Cellar/gradle@7/7.6.3 # for arm64 or /usr/local/Cellar/gradle@7/7.6.3 for x86_64. Make sure the version is up to date
export LOCAL_DEV_BUILD=1
export PATH=$ANDROID_HOME/ndk/25.2.9519653/toolchains/llvm/prebuilt/darwin-x86_64/bin/:$PATH # prepend android studios latest ndk to the path so it's clang gets used to build geckoview. NDK 25.2.9519653 uses clang 14.0.7, ideally we'd use clang 16 (to be the same as Linux) but that's not an option yet for android studio. NDK 26.1.10909125 uses clang 17.0.2, which we should evaluate with the esr128 migration
```

Make sure that the variables defined are all valid for your setup, and that PATH is now using our newly built clang.

<details>
<summary>You may want to run `./mach bootstrap` the first time to get some deps</summary>

Select "3", installing deps as recommended. This will also populate some ~/.mozbuild files and downloads. DO NOT let it setup your git, it can just as easily accidentally break it, so select 'n'. Likewise it's probably best to select 'n' when asked about submitting code to Mozilla. We currently maintain our own configs so also select 'n' for overwriting mozconfigs.
</details>

To build:

```
cd tools/geckoview
make clobber
make config
```

<details>
<summary>You may need to use `rustup` to install and select an appropriate rust version and target.</summary>

```
rustup default 1.69
rustup target add aarch64-linux-android
```
</details>

then build (this may take a while depending on your machine, ranging from 10 minutes to over an hour as of 2023)

```
make build
```

### Packaging and publishing Geckoview for use in Android

we make the fatarr and publish to our local gradle repository with:

```
make package
```

and after all this we should be able to confirm by running  `make show` and seeing our new files

### x64 builds

All the make commands can take an `ARCH` override such that you can

```
make clobber ARCH=x64
make config ARCH=x64
make build ARCH=x64
make package ARCH=x64
```

## Building firefox-android

Get a copy of the repo [firefox-android](https://gitlab.torproject.org/tpo/applications/firefox-android) and check out the relevant build branch (eg: firefox-android-115.2.0-13.0-1)

### Android Components

edit `android-components/plugins/dependencies/src/main/java/Gecko.kt` and change:
- `const val version = "115.0.20230710165010"` to have the correct version that matches what the tor-browser/geckoview version, for instance you might have to change the version to "115.2". The date part should have been previously copied from here to android-env.sh in tor-browser in the previous step. You can confirm you have it right with `ls ~/.m2/repository/org/mozilla/geckoview/geckoview-default-omni` which will show you what geckoview published. You need to match your target
- `NIGHTLY("geckoview-nightly-omni"),` to `NIGHTLY("geckoview-default-omni"),`
- `val channel = GeckoChannel.RELEASE`  to `val channel = GeckoChannel.NIGHTLY` if it's not like that already.

then open `android-components/local.properties` and populate it with the following

```
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.

sdk.dir=/home/YOU/Android/Sdk
dependencySubstitutions.geckoviewTopobjdir=/FULL/PATH/TO/tor-browser/obj-arm-unknown-linux-androideabi
dependencySubstitutions.geckoviewTopsrcdir=/FULL/PATH/TO/tor-browser
```

remeber to set the variables to your local setup

then open `android-components` in android studio and build it (`Build > Make Project`)

### Fenix

Edit `fenix/gradle.properties` and add `org.gradle.java.home=/usr/lib/jvm/java-11-openjdk-amd64/` to the bottom (or the proper path for your system).

Then open `fenix/local.properties` and fill as follows

```
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.
#Tue Jun 27 16:08:20 PDT 2023

sdk.dir=/home/YOU/Android/Sdk
dependencySubstitutions.geckoviewTopobjdir=/FULL/PATH/TO/tor-browser/obj-arm-unknown-linux-androideabi
dependencySubstitutions.geckoviewTopsrcdir=/FULL/PATH/TO/tor-browser
autoPublish.android-components.dir=../android-components
```

#### Fetching required build artifacts

To build the final tor browser for android a few other dependencies are needed: `android-release.aar`, `service-release.aar`, `universal-0.0.3.jar`, `jsocksAndroid-release.aar`, and a `noscript.xpi`. We build these in [tor-browser-build](https://gitlab.torproject.org/tpo/applications/team/-/wikis/Development-Information/Tor-Browser/Building) and we have a script to harvest them and place them in the right places inside the fenix build directories. If you have a local tor-browser-build, in the same parent directory of the firefox-android repo directory, you can call `tools/tba-fetch-deps.sh` from the `fenix` directory.

If you cannot run tor-browser-build locally you will need to set it up somewhere else (like the build servers) and run an android build to generate the required files, or get someone else to. Then you will need to copy them into a local skeleton of tor-browser-build so that the `tools/tba-fetch-deps.sh` can find them. The files you need and structure to recreate is as follows

```
tor-browser-build/out/
    tor-android-service/tor-android-service-xxxxxxxxxxxx-xxxxxx/
        jsocksAndroid-release.aar  
        service-release.aar
    tor-onion-proxy-library/tor-onion-proxy-library-xxxxxxxxxxxx-xxxxx
        android-release.aar  
        universal-0.0.3.jar
    browser
        noscript-xx.x.x.xpi
```

#### Building

Open `fenix` in android studio and build (`Build > Make Project`).

#### Generating non dev builds (nightly, beta(alpha), release)

As an alternative, you can also build from the command line:

```
./gradlew --no-daemon -Dorg.gradle.jvmargs=-Xmx20g -PversionName="$variant" assemble$variant
```

where `$variant` must be one of `Release`, `Beta` or `Nightly` (notice the capital letter to make the result camelCase).

#### Running

From Android Studio with `fenix` loaded simply select from the menu `Run > Run 'app'` with an appropriate android device with USB debugging enabled plugged in.

If you have built a specific *variant* for firefox-android as shown above, you can sign the apks with the same QA key we use for the builds from tor-browser-build with `fenix/tools/tba-sign-devbuilds.sh`.
This binary assumes that you have `tor-browser-build.git` at the same level of `firefox-android.git`, but you can override this with the `TOR_BROWSER_BUILD` environment variable. Then you can use `adb` to install the package with something like `adb install ./app/build/outputs/apk/fenix/beta/app-fenix-arm64-v8a-beta-signed.apk`

##### For MacOS:

For quickly building, aligning, signing, and installing (to an emulator or device plugged in) the following is useful. Replace the values for variant and Variant with the variant of your choice. Also point the apksigner to your keystore, see https://developer.android.com/studio/publish/app-signing#generate-key for generating a key.
```
export Variant=Beta && export variant=beta
./gradlew clean --no-daemon -Dorg.gradle.jvmargs=-Xmx20g assemble$Variant
cd app/build/outputs/apk/fenix/$variant
zipalign -v -p 4 app-fenix-arm64-v8a-$variant-unsigned.apk app-fenix-arm64-v8a-$variant-unsigned-aligned.apk
apksigner sign -ks ../../../../../../my-release-key.jks --out app-fenix-arm64-v8a-$variant.apk app-fenix-arm64-v8a-$variant-unsigned-aligned.apk
adb install app-fenix-arm64-v8a-$variant.apk
cd ../../../../../..
```

An example section for `~/.zshrc` to get it to run
```
export JAVA_HOME=/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home/ # or your equilivent
export ANDROID_HOME=$HOME/Library/Android/sdk 
export PATH=$PATH:$ANDROID_HOME/platform-tools/ 
export PATH=$PATH:$ANDROID_HOME/build-tools/34.0.0/
```

The last line assumes "Android SDK Build-Tools 34" is installed
![Screenshot_2023-11-09_at_15.18.22](uploads/759156213ed8d7ef392845d4aa88fb7d/Screenshot_2023-11-09_at_15.18.22.png)