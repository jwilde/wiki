# Improving Tor Browser for Android Development Processes

## Facilitator(s): dan_b+clairehurst

## Issues
- https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42227
- https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41000
- 
## Topics

- Improving Android Development Processes
    - patch process and strategy
    - automated testing
    - official (more or less) distribution of GV and other artifacts
    - Allow developers clone only firefox-android.git, without cloning tor-browser.git.
      - Getting tor-android-service and tor-onion-proxy-library is easy only if you have tor-browser-build
      - Maybe useful also for the CI (we have another session for that, but maybe we can already discuss some details to be ready for that session)
    - GK suggests talking about going to RR again
      - Some details about what we used to do before 102 might be useful here, but also in the context of the first point.
      - Pain points we had:
        - Keeping the same pace as Mozilla is hard, very hard. If you find a crash or other nasty bug created by our patches, or worse, by the differences between Mozilla's and ourconfiguration you risk of losing it
        - E.g. no telemetry, and you know how Firefox for Android is full of telemetry
       - We also need to keep pace on audits.
    - Crazy ideas (that got discarded in the past and reasons for them to be discarded):
      - Switch to Focus instead of Fenix. It might reduce the load of changes and audits to do. But single tab and no addons (so NoScript, hence no Security Level)
      - Use the reference browser and implement the rest of the UI. Possibly too much effort.
    - PieroV suggests that automation in rebases (and related automated tests) would be extremely helpful also for this
    - If we have to do audits for Android, we could also try to create a RR desktop channel (maybe not to be considered stable)

## Notes

### Patchset

1. We survey our current patchset and find what our patches do
2. We remove old patches and create a proposal on how we want to reorganize them
3. We iteratively check MRs to do so

Timeline: just after the sponsor 96 work

We decided not to try other approaches until we do this work.

### Make developer lives easier

- Precompiled artifacts in a Maven/Gradle repo
  - GV
  - Tor stuff
  - Android components? They should be easy enough to use (just add a line in local.properties?)
  - Maybe we can do something to make it the default

We will need someone (Claire? :)) to setup a solution to consume them (e.g., something in Gradle, or a shell script if we can't).

### CI

The build should be easy enough to pull.
We already do in tor-browser-build, we can remove the dependency handling since CI doesn't need to be reproducible.
If we manage to do the previous point, we should get to a point where we can build on the CI pretty easily.

We could add stuff like linting and static analysis.
It might also add a lot of noise and end up being ignored, which is something we should totally avoid (we should do it good from the beginning).
It's somehow relaed to the patchset discussion, but it's something we'd need to investigate.

### Testing

Part of fixing the patchset is also fixing the tests, at least to get started.