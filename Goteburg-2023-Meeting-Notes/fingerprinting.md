# Fingerprinting Mega-Chat

## Facilitator(s): thorin

## Issues

- https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42225
- https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42226

## Topics
- Fingerprinting!
  - Explain Security sliders impact on fingerprinting + why you should not rely on amiunique, coveryourtracks, etc.
  - fingerprint health report/test tooling
- Pref organisation: one massive pref commit or individual alongside relevant commits
  - example: the pref to disable the firefox-view tabbar button; does it belong in the pref commit or in the remove about:firefoxview commit?
- How to stop blocking accessibility features.

from the night before:
- what missing simple FP protections or compat improvements can we add in 13.5
  - e.g. return rounded up integers with min- in css/matchmedia/@media queries for screen/window (to remove subpixels)
  - e.g. block FontSubstitutes registry read in windows
  - e.g. add Arial, Courier New, Times New Roman aliases in Linux
- upstream new and past patches otherwise rebase hell will ensue
- nudge/push upstream issues - leverage moz devs and goodwill
  - especially ones already authored by moz but leak or easily swappable values
  - e.g. moxInputSource leaks, change maxTouch to 10 etc
  - e.g. subpixel leaks in css/media via min-*
- assess past APIs/issues that were never analysed/tested
  - those that need no action, i.e they are equivalancy or no extra entropy, then enable the API
  - from monday: how do we assess FP threats

---

## Notes
- if you have a binary choice, but one is more probable
- information theory:
  - information = -log probability
  - entropy = E[information]
- single choice -> 0 bits of entropy
- binary choice 50:50 -> 1 bit of entropy
- binary choice 10:90
  - the 10% user has more information than the 90% user. This distribution has more entropy than a uniform distribution

#### fingerprinting defenses
- the key is hide the real value
  - lie about it (persistent or randomize or randomize per eTLD+1 per session etc)
     - randomizing will confuse naive scripts but all solutions must assume an advanced script
        - an advanced script can always detect randomness e.g. via a third party
     - randomization needs to be smart, because it's a white box model, so we want to make it difficult to write an estimator to reduce the noise introduced by our randomization
       - fixing the parameters for each first party might help (to avoid having repeated different measurements that could be used to remove the noise)
  - disable the API
  - block known scripts (e.g. uBO)
    - enumerating badness: manual allow/block listing of known bad actors, but requires constant updating, is necessarily imperfect; better to fix the underlying issues
    - long-term game of whack-a-mole
    - it helps (adblocks are already doing this job)

#### current worst vectors (in no particular order)
  - fonts
    - we're in a pretty good shape at the moment, we have the allow lists and we bundle fonts, no other browser is doing the same
  - subpixels
    - methods to measure and things to measure are legion
    - things affected are legion
    - examples of things that influence subpixels:
      - system scaling
      - device pixel ratio
      - zoom (but forget about this, we can't do much, even though this introduces math changes)
    - Thorin's idea: constantly jitter the zoom
      - Every time you load a new domain, you don't render the page at the set zoom level (well, 100%) but you do for example 100.5%, 99.5%, etc.
      - You keep the same jitter per session, per eTLD+1 + scheme, and per window type (PB/normal) so an attacker can't repeat measurements to remove the noise (protect the seed, stop averaging).
  - webgl
    - lots of stuff to do and even investigate what
  - user changes
     - e.g. changing web-content language(s) and order
     - research would be useful for identifying how often *real* people are actually changing prefs

#### user impacts
- **two** impacts types:
  - known **usability** (website doesn't break, but does not behave as the user expects/wants)
    - e.g. wrong timezones for examples
    - e.g. accessibility issues
  - known **breakage** (loss of functionality or API breaks)
    - mismatched user-agent http headers
      - JS returns 'correct' user-agent string
      - HTTP returns windows or android
      - The biggest reason not to have the real platform in the HTTP header is server logs: the user-agent is often logged by webservers.
    - canvas-read is totally randomized
       - but we have per-session canvas-read site exceptions 
    - 60 fps timing locking
      - 120/144 hz monitor people are sad
        - Firefox doesn't probably support adaptive sync/freesync/g-sync :(
    - timezone spoofing triggres anti-bot/anti-fraud
    - webgl
    - modifier keys
      - 1222285 & 1433592 - spoof keyboard events and suppress keyboard modifier events (FF59)
        - Spoofing mimics the content language of the document. Currently it only supports en-US.
        - Modifier events suppressed are SHIFT and both ALT keys. Chrome is not 
    - accessibility
      - we could swap out the `Colors > Manage Colors...` menu to allow dark themes, not allow too much fine tuning w/o sort of explanation/gate