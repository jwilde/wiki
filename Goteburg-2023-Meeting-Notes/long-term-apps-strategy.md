# Long Term Applications Strategy

## Facilitator(s): richard

## Goals for this Session

- Outline for a `Five-Year Plan` accross our apps
    - Tor Browser Desktop
    - Tor Browser Android
    - TorVPN
    - torbrowser-launcher
    - Base Browser
    - Mullvad Browser
    - arti

## Topics

- TorVPN initial release Summer 2024
- arti!
- Tor Browser Desktop will need to simultaneously support for some window of time:
  - legacy tor daemon
  - in-proc arti
  - out-of-proc arti?
- Tails+Whonix will need to transition from tor to arti
- TBA will need to support both arti and TorVPN
- Do we care about Orbot support in TBA?
- TorVPN
  - onion services on mobile

## Timeline

### 13.5 (Q2 2024) - 7 months development time

#### Android:
- Connection Assist ctor (Jan 2024)
- Try to use HTML+JS on Android
- Start tor with legacy libraries and plumb down the control port to backend JS
- Connection settings revamp (TBC, lower priority)
- 13.5.x
  - tor VPN integration support? summer 2024

### Desktop:
- Lox (13.5 alpha only)
- Connection Assist improvements
- Conflux circuit display work
  FIXME(ahf): Talk with Mike Perry about this.
- Betterboxing (low priority)
  - Ready for handoff to developers (new UI setting and some restyling to letterboxing) (valuable for Mullvad as well)
- Convert onion site errors to new neterror template (low priority)
- Fixes for error reporting and other stuff for TorConnect and TorSettings (might happen also while we port it to Android)

#### Mullvad Browser:
- 13.0 uncompleted items
- web authentication (webauthn)?
- Code audit + possibly some modifications to code
- system install (Windows + Linux)
  - some prototyping has been done
- flatpack
  - Unofficial already exists, we want to check its fingerprint is the same as the official one, and probably ask to control it and make it official
- default browser
  - some prototyping has been done
- 3rd party protocol handlers (mailto:, magnet:, etc)
- improvements to the development process (e.g., nightly builds)
- will also benefit from betterboxing

### 14.0 (Q3/Q4 2024) - ESR update: 5 months cycle

#### Android:
- ESR 128
- bug fixes
- audits

#### Desktop:
- ESR 128
- bug fixes
- audits
- Burn button instead of new identity

#### Mullvad Browser:
- ESR 128
- bug fixes
- audits
- Burn button instead of new identity

### 14.5 (Q2 2025)

#### Android:
- Arti transition
- Connection Assist Arti (Android)
- Lox (potentially ship)

#### Desktop:
- Privacy & security settings cleanup (low priority)
  - Most scary, not designed yet ---> the thing that we might want to move to later
 - Users are already using these "unsupported" features, and we'll need migration or explanations
- Lox (potentially ship)
- linux packages? 

#### Mullvad Browser:
- enhanced extensions (candidate to be pushed, if it cannot be done)
  - calling out extensions that we def. know are fingerprintable (manifest analysis)

### 15.0 (Q4 2025)

#### Android:
- ESR 14X

#### Desktop:
- ESR 14X

#### Mullvad Browser:
- ESR 14X

## 5 year strategy

### Product:
- We have VPN clients for Android and desktop
- Alternative browsers to Tor Browser are available to use with Tor VPN
- Desktop and Android are transitioned to Arti
- Android has rough feature parity with desktop
- Tor users can VOIP
- Censored users are benefitting from lox
- Better bridge lines
- Closer noscript integration
- We don't include features in Privacy & Security that we don't support
- Users can install browser extensions
- Betterboxing
- Onion plan
- Circumvention UX in arti (aka arti "batteries included")

### Meta:
- VPN development has been handed over to the Applications team, or a new team is created
- We have multi-year roadmaps with clear goals
- We have a solid design to development pipeline, with designs completed ahead of the development cycle

## Links

- Tor Browser Team Vision: https://nc.torproject.net/apps/onlyoffice/471898?filePath=%2FApplications%20Team%2F2023%20Applications%20Team%20Vision%20by%20Richard.docx
