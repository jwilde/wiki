# Tor Browser and Mullvad Browser Threat Modeling

Main pad: https://pad.riseup.net/p/tor-applications-goteburg

## Facilitator(s): richard

## Background

### Tor Browser

#### Features/Protections

- Disk Leak Protections
  - goal: make it possible to delete the TBB and with it all trace of TBB having been on the computer
  - ultimately unachievable on non-ephemeral platforms
  - we also promised no way to tell that the browser has been used, for plausible deniability, but it's also impossible (e.g., prefs left behind, last time updates were checked, etc...). Should we still consider traces of TBB on a computer a disk leak, or are only the traces about content to be considered disk leaks?
  - always-on private browsing mode
  - in memory cert store
  - in memory cache for video
  - certainly others
- IP/Location Anonymity
  - goal: user traffic not traceable back to their 'real'/fuzzy locaiton based on IP
  - tor integration
  - proxy bypass protection patches
- Cross-Site Tracking/Linkability Protections
  - goal: prevent cross-webite tracking/correlation of user activity between tabs, between apps
  - disabling of telemetry, 3rd party services
  - first party isolation
  - resist fingerprinting
  - linkability patches (download panel, disable uri handlers, etc)
- Security by disabling features
  - Security Level
  - disables various things at various levels on a global basis
  - the design model for this tried to separate privacy and security
  - I.e., if it's for privacy it must be for everyone
  - The border is quite fuzzy, because privacy is part of security, after all, and stuff like disabling JS improves privacy, even though that isn't the first goal of the feature
- Censorship Circumvention
  - vanilla tor bypassing local content filtering (eg home/school/corporate network filters)
  - PTs + auto-censorship-circumvention for tor-blocking entities (network admins, governments, etc)

#### Users

- personas: https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/tor-personas.pdf
  - note: out of date/a bit innacurate according to donuts but its the best we've got
- who are our users?
  - journalists
  - whistleblowers
  - hacktivists
  - leakers
  - vulnerable populations (lgbtq, women, children)
  - tech enthusiasts
  - I would replace this with privacy concerned users, or at least add its own category
  - casual users who heard of Tor and want to try it in Tor Browser
  - or people that wants to try Tor/Tor Browser as a free alternative to VPNs after seeing so many VPN ads
  - maybe not users we target, but they exist
- ^these folks all have different threat models ranging from inteligence agencies to their parents

### Mullvad Browser

#### Features/Protections

- Enabling webrtc What is the difference between TB and Mulvad when this is the case When we do isolation primitives, first party isolation, then there needs to be an RPC mechanism between the application and the network provider How we handle onion services: we make them the same as HTTPS, which is not in Mullvad. If the onion work that rhatto is doing will potentially change this. Tor Browser will always transport over Tor, there is no plan to make it so you can run TB without Tor Have same fingerprint as Mullvad Browser and Tor Browser?
- If we lock off the branding, both should be fairly identical. If we put webrtc and webaudio into TB, then they will have the same default level from a JS fingerprint side.
- The idea would be to have more unified engineering work to increase the pool of users with the same fingerprint
- Does not mean that we would have the same threat model, but if we are starting in the place where we have the same fingerprint (at least the default experience).
- Confusion from users about the 'security selection' how that doesn't match the privacy selection. Make these match/aligned with threat models. If this is my threat model then I should use this setting, instead of just being confused about what to set things to.

Communicating with users the difference between privacy and security. We've always sort of promised that you get the same amount of privacy but different security, but that isn't always true. The security level is an abstract thing that people screw around with until the website works, which isn't great.

Giorgio posits a 'public browsing' mode which is disposible, so when you close the tab everything is forgotten. Pierov says we should collaborate with Mozilla and Tom on this, but this is tied to sites/origin so its a bit orthogonal.

How would users know when to use this? How do you know when its safe to use it? If you aren't technical, it could be a foot gun. \<you could warn them that this is not safe and then they can make a decision... what are you going to do, just go and use another browser\>. We lose users when a site is broken because of the browser.

This is relevant to the threat models. If you are an activist/whistleblower whatever, you don't want them to go to that site. If you are not a technical user, you are going to open up chrome and do whatever they were going to do anyways.

Go into different threat models, and then fit the different security models into that.

Security slider is restricted to security, change it so it includes privacy things.

What happens when a user wants high security, but does want persistence? In that case its not a slider anymore. We need to think about the percentage of the users who have these needs and what are their technical capabilities.

Mullvad sets of users: Have not done user research since releasing feedback has been emails to support and paying attention on social most people who are using it are doing so to improve their privacy fighting against tracking and mass surveillance, which is not surprising because this is how mullvad advertises itself security slider generates confusion, some users want most privacy and security, so they go to the most safest. expect that if sites break they reduce the settings like tor browser users do We've been clear if you have a higher threat model, you should be using Tor Browser. Even power users don't know where to set the slider. Its not even documented when you should use it and not use it. Safest gives you the NoScript button and allows you to temporarily relax temporarily different things. We dont try to communicate to users why a website is broken (NoScript does try to do this). Would be nice to have a feature to report issues. If we go with this long-term idea of ma1 of having a way to "make this tab work"

1. get rid of security slider and only have standard
2. put everything on safest and provide UX to allow users to enable things to unbreak it. this makes you see the noscript logo on websites, when normally its hidden
3. relaxed mode: would have things like 'retain passwords' and keep site logins, etc. this is different from unbreak a website
4. unbreak this site ("public browsing mode" as an inverted private browsing mode)
5. threat model slider: rui says this would be the simplest for users. <there are all sorts of decisions about what would have to go in here, adding other factors to this one slider doesn't make sense. it would make more sense to have on-boarding process, do you want things to leak to the disk?). Onboarding would be digesting things down to the base things. <If we manage to have 3-4 personas for onboarding and some elasticity for changes during temporary sessions, then we will have a nice compromise between usability and fewer buckets of usability>
6. make security model more responsive, by putting an indicator on the slider to let you know that a website is doing something on this webpage

rui: mullvad has a strong preference to not have any onboarding, just download it and use it. it just works without a lot of choices. but still want to give the possibility for people who, depending on their threat model want to relax or harden them. this is a UX problem. not giving people hurdles to straight away and use the browser.

thorin: i agree with the idea that as an end user you just want to flick it on and use it and have it. a lot of friction could be removed depending on how we label things. you don't need to go into the threat model, you could learn more and hide it, etc. maybe: here is a suggested mode, if things break, relax, if you want things more private you go the other way. maybe its a compatibility slider.

gk: banking site is a good example. do we have data about breakage if its related to fingerprinting or privacy issues, or tor itself. often its the network banking. if we go a long way to adjusting slider modes, then we still have a different problem.

We do want people to use tor browser daily, rather than one-off.

henry: I know its more purposeful rather than a general purpose browser, I know that I may need to use FF for other things

rui: hope that improvements happen in TB as well, don't want to create more work to have these differences. Tor has different types of users as a non-profit vs. mullvad which is corporate.

thorin: people who have gone to the trouble of downloading TB, people have done it with an intent and a purpose. 2009 tor users - who uses tor browser, "normal people use it, librarians, law enforcement etc." but its also what you use if you are a whistleblower. but people also go to use it for specific purposes, and it does it it well, but people try and push it as a general purpose browser. there is a mixed message

richard: its not our desire that TB users are using it in an intentional way, we'd like a general purpose usage, but because of usability trade-offs, its not possible to use it for many things. the reality is that is how people use tor browser, but that isn't because we want that type of use, its outside of our control.

mismatch of reality and perception.

- Disk Leak Protections
- Cross-Site Tracking/Linkability Protections
- Security by disabling features
- Ad-blocking

#### Users

- presumably mostly privacy-focused indiviuals that want to use the ordinary internet but we really don't know

## Current Problems

- we don't have any rigorous heuristics for evaluating loss of functionality versus 'protections' we provide
- disk leaks: we advertise/implicitly promise no-trace-left behind on uninstall
  - packaging: non-standard installation method is a hurdle for non-technical users
  - inconsistent policy: saving bookmarks ok, saving history+cookies isn't
- lots of people run tor-browser (and presumably mullvad-browser) w/o PBM on, but we don't 'officially' support it
- linkability concerns go against user expectations, aren't telegraphed to the user:
  - external uri handlers for example
- accessibiliity \<-\> anti-fingerprinting tension

# Goals for this Session

- Rubric for making decisions around features which improve privacy||security but affect usability
  - default behaviour or somehow gated
  - how configurable?
  - not configurable
  - about:preferences
  - about:config
  - locking preferences in stable vs alpha
  - accessibility
- Outline for an improved Tor Browser Threat Model Doc Describing Features:
  - Anonymity
  - Anti-Tracking
  - Anti-Fingerprinting
  - Security
  - Disk Leaks
  - Possibly seperate ones (or sub-sections) for:
  - Tor Browser Android
  - Tor Browser on Tails
- Outline for a new Mullvad Browser Threat Model Doc

# Notes Monday 23

The current model hasn't been updated for many years (at least 7-8 years). It mentions lots of stuff that aren't relevant anymore (e.g., flash, Java). Now we have also Mullvad Browser, which uses the same model as Tor Browser, but it would need its own. Also, the disk leak section is very outdated (see the description above).

Question: where is the current/old document? Where are we adding the new one? - https://2019.www.torproject.org/projects/torbrowser/design/

The original doc is too long to go throguh during these two sessions, so we're going to discuss some of the topics.

We use PBM to reduce the traces left on the machine. Lots of people hate this, but it's very useful. Tor Browser was taught for a lot of people, including activists and whistelblowers, so it was important not to let anyone see what the did. Then Tails arrived, and it's a better solution for people that need absolute disk sanitazion. Most of the unexpected problems is disk leak. E.g., there's the operating system doing "clever" stuff and we don't expect it to. So, if you want to be sure not to have any disk leak, you should use Tails (and especially avoid Windows :P). We should have a meta issue about this kind of issues on GitLab. Moreover sometimes fixing the leaks after the facts is extremely difficult.

Question: can you have multiple threat models? Yes, but enabling/disabling features could be fingerprinted and you create even more buckets.

Thorin says that the biggest change of Tor Browser/Mullvad Browser vs. Arkenfox is PBM.

In our threat model, we're too promising. We should do the best we can and try to do it. E.g., we can't promise to make it appear that you've never had the browser installed. And, for example, SecureDrop users can hide in the crowd of people using Tor for other purposes.

Basically, we should re-define disk leak. The new definition should be about the content/your sessions, not about the program itself. It should not include update history, timestamps and so on (so, what was called the protection about your habits, we can't really do that).

We might get some backlash, but as a matter of fact that's already the reality. So, it's an educational change. Some users will say that we're failing to do what we promised. Our response can be only to use Tails, if it's in the user's needs/threat model.

Is stuff encrypted with a key that then is forgotten okay for us? E.g., IndexedDB. The key should be marked not to go to swap at least. Also, the file can be unlinked immediately and be still available until the last process using it is open, then immediately deleted by the OS.

Alex mentioned that we shouldn't try to protect Edward Snowden or similar specific targets, but we should protect classes of users (e.g., people who want to abort, domestic violence, etc).

what is this? - https://2019.www.torproject.org/about/torusers.html personas - https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/tor-personas.pdf

Having some rubric about e.g., fingerprinting could help a lot on the development. We could analyze new possible threats by checking that.

# Notes Tuesday

 Would it be possible to make the fingerprints of Tor Browser and Mullvad Browser the same? At the moment the biggest difference is WebRTC and related stuff (WebAudio, etc...). But Tor Browser might have WebRTC eventually as well. At that point the differences will be uBlock bundled by default (and we might change it) and the onion treated as HTTPS. The rest is branding, so not exposed (more or less, looking at you about:logo).

That will be the default experience for Mullvad Browser, but then it might be changed by users, depending on the thread model. Rui says it would be nice to tie the security level to the expected threat model.

We would like to have an "unsafe" tab with relaxed preferences to avoid users going away from our browser and not coming back. Another idea is having a relaxed mode, that for example doesn't have PBM by default.

We're missing data about what users expect. Rui looks for feedback on social media, but apart from that they only give the possibility to send emails to support@mullvad.net only.

In general, users are looking for more privacy, or else they'd be using Firefox or another browser.